use serde::{Deserialize, Serialize};

/// enum representing the SQL command
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum SqlCommand {
    Select {
        columns: Vec<String>,
        filters: Option<LogicalOperator>,
        table: String,
        group_by: Option<Vec<String>>,
    },
    Insert {
        table: String,
        columns: Vec<String>,
        values: Vec<Value>,
    },
    Update {
        table: String,
        columns: Vec<String>,
        values: Vec<Value>,
        filters: Option<LogicalOperator>,
    },
    Delete {
        table: String,
        filters: Option<LogicalOperator>,
    },
    Create {
        table: String,
        columns: Vec<String>,
        column_types: Vec<ValueType>,
    },
    Drop {
        table: String,
    },
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Filter {
    pub key: String,
    pub value: Value,
    pub operator: Operator,
}

//TODO change the name
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum LogicalOperator {
    And(Box<LogicalOperator>, Box<LogicalOperator>),
    Or(Box<LogicalOperator>, Box<LogicalOperator>),
    GroupedCondition(Box<LogicalOperator>),
    Filter(Filter),
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Hash)]
pub enum BooleanOp {
    And,
    Or,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Hash)]
pub enum Operator {
    Eq,
    NotEq,
    GreaterOrEq,
    Greater,
    LessOrEq,
    Less,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum Value {
    Int(i64),
    Float(f64),
    String(String),
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Hash)]
pub enum ValueType {
    Int,
    Float,
    String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Table {
    pub name: String,
    pub columns: Vec<Column>,
    pub indexes: Vec<Column>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Column {
    pub name: String,
    pub col_type: ValueType,
    pub is_index: bool,
}
