pub(crate) mod types;

use crate::error::ParseSQLError;
use pest::iterators::Pair;
use pest::iterators::Pairs;
use pest::Parser;
use pest_derive::*;

use types::*;

#[derive(Parser)]
#[grammar = "sql/grammar.pest"]
struct SqlParser;

/// parses a string into a SQL command
pub fn parse(command: &str) -> Result<SqlCommand, ParseSQLError> {
    // we always get a sql rule that has select, insert, create or delete in it
    // something like:
    //    sql ->
    //        select ->
    //            column, table, filters
    let sql: Pair<Rule> = SqlParser::parse(Rule::sql, command)
        .map_err(|e| ParseSQLError::GenericError(e.to_string()))?
        .next()
        .unwrap(); // this is an Optional

    // gets the inner object that is what we need
    let sql = sql.into_inner().next().unwrap();

    match sql.as_rule() {
        Rule::select => parse_select(sql.into_inner()),
        Rule::delete => parse_delete(sql.into_inner()),
        Rule::insert => parse_insert(sql.into_inner()),
        Rule::update => parse_update(sql.into_inner()),
        Rule::drop_table => parse_drop_table(sql.into_inner()),
        Rule::create => parse_create_table(sql.into_inner()),
        _ => Err(ParseSQLError::GenericError(format!(
            "cannot parse {:?} as sql command",
            sql
        ))),
    }
}

fn parse_create_table(mut pairs: Pairs<Rule>) -> Result<SqlCommand, ParseSQLError> {
    let table = parse_table(pairs.next().unwrap().into_inner())?;
    let (columns, column_types) = parse_create_columns(pairs.next().unwrap().into_inner())?;

    Ok(SqlCommand::Create {
        table,
        columns,
        column_types,
    })
}

fn parse_create_columns(
    pairs: Pairs<Rule>,
) -> Result<(Vec<String>, Vec<ValueType>), ParseSQLError> {
    let mut columns = Vec::new();
    let mut types = Vec::new();
    for pair in pairs {
        // the rule for create_column is the inner be equal to
        //      "column type"
        let mut create_column = pair.into_inner();
        let column = create_column.next().unwrap().as_span().as_str();
        let val_type = parse_val_type(create_column.next().unwrap())?;

        columns.push(column.to_string());
        types.push(val_type);
    }
    Ok((columns, types))
}

fn parse_val_type(pair: Pair<Rule>) -> Result<ValueType, ParseSQLError> {
    let val_type = match pair.as_span().as_str().to_lowercase().as_str() {
        "int" => ValueType::Int,
        "float" => ValueType::Float,
        "string" => ValueType::String,
        _ => return Err(ParseSQLError::GenericError("unknown type".to_string())),
    };

    Ok(val_type)
}

fn parse_drop_table(mut pairs: Pairs<Rule>) -> Result<SqlCommand, ParseSQLError> {
    Ok(SqlCommand::Drop {
        table: parse_table(pairs.next().unwrap().into_inner())?,
    })
}

fn parse_table(mut pairs: Pairs<Rule>) -> Result<String, ParseSQLError> {
    Ok(pairs.next().unwrap().as_span().as_str().to_string())
}

fn parse_update(mut pairs: Pairs<Rule>) -> Result<SqlCommand, ParseSQLError> {
    let table = parse_table(pairs.next().unwrap().into_inner())?;
    let mut filters = None;
    let mut set_vals = None;
    for pair in pairs {
        match pair.as_rule() {
            Rule::set_vals => set_vals = Some(parse_set_vals(pair.into_inner())?),
            Rule::where_filters => filters = Some(parse_where(pair.into_inner())?),
            _ => {
                return Err(ParseSQLError::GenericError(
                    "invalid rule while parsing update".into(),
                ))
            }
        }
    }

    let (columns, values) = set_vals.unwrap();

    Ok(SqlCommand::Update {
        table,
        filters,
        columns,
        values,
    })
}

fn parse_set_vals(pairs: Pairs<Rule>) -> Result<(Vec<String>, Vec<Value>), ParseSQLError> {
    let mut columns = Vec::new();
    let mut vals = Vec::new();
    for pair in pairs {
        // the rule for set_vals is the inner be equal to
        //      "column = val"
        let mut set_val = pair.into_inner();
        let column = set_val.next().unwrap().as_span().as_str();
        let val = parse_val(set_val.next().unwrap().into_inner())?;

        columns.push(column.to_string());
        vals.push(val);
    }
    Ok((columns, vals))
}

fn parse_insert(mut pairs: Pairs<Rule>) -> Result<SqlCommand, ParseSQLError> {
    let table = pairs.next().unwrap().as_str().to_string();
    let columns = parse_columns(pairs.next().unwrap().into_inner())?;
    let values = parse_values(pairs.next().unwrap().into_inner())?;
    Ok(SqlCommand::Insert {
        table,
        columns,
        values,
    })
}

fn parse_delete(pairs: Pairs<Rule>) -> Result<SqlCommand, ParseSQLError> {
    let mut table = String::new();
    let mut filters = None;
    for pair in pairs {
        match pair.as_rule() {
            Rule::table => table = parse_table_name(pair.into_inner())?,
            Rule::where_filters => filters = Some(parse_where(pair.into_inner())?),
            _ => {
                return Err(ParseSQLError::GenericError(format!(
                    "not expected rule in delete {:?}",
                    pair.as_rule()
                )))
            }
        }
    }

    Ok(SqlCommand::Delete { table, filters })
}

/// parses the select stmt, returning a SqlCommand::Select
fn parse_select(pairs: Pairs<Rule>) -> Result<SqlCommand, ParseSQLError> {
    let mut columns = vec![];
    let mut table = String::new();
    let mut filters = None;
    let mut group_by = None;

    for pair in pairs {
        match pair.as_rule() {
            Rule::columns => columns = parse_columns(pair.into_inner())?,
            Rule::table => table = parse_table_name(pair.into_inner())?,
            Rule::where_filters => filters = Some(parse_where(pair.into_inner())?),
            Rule::group_by => group_by = Some(parse_columns(pair.into_inner())?),
            _ => {
                return Err(ParseSQLError::GenericError(format!(
                    "the rule {:?} is not allowed in a select",
                    pair.as_span()
                )))
            }
        };
    }
    Ok(SqlCommand::Select {
        columns,
        filters,
        table,
        group_by,
    })
}

fn parse_where(pairs: Pairs<Rule>) -> Result<LogicalOperator, ParseSQLError> {
    let mut filter_left = None;
    let mut filter_right = None;
    let mut logical = None;
    let mut final_op = None;

    // walks the tree as it was a stack
    // first get the right element, if it only has one,
    // that is our final where condition
    // if we have more, we parse the second-last element (either and or or)
    // and combine it with the third-last.
    // we keep doing this until we don have anything left in our iterator (stack)
    for pair in pairs.rev() {
        match pair.as_rule() {
            Rule::condition => {
                if filter_right.is_none() {
                    filter_right = Some(LogicalOperator::Filter(parse_filter(pair.into_inner())?));
                } else {
                    filter_left = Some(LogicalOperator::Filter(parse_filter(pair.into_inner())?));
                }
            }
            Rule::logical_operator => logical = Some(parse_logical_operator(pair)?),

            Rule::grouped_condition => {
                // grouped condition is very similar to how we parse where
                // the only real difference is that we should evaluate it together
                // in the higher levels
                // also because sql is a declarative language and not imperative
                // there is no guarantee that we will execute something in the grouped condition
                // before!
                let temp = parse_where(pair.into_inner())?;
                let temp = LogicalOperator::GroupedCondition(Box::new(temp));
                if filter_right.is_none() {
                    filter_right = Some(temp);
                } else {
                    filter_left = Some(temp);
                }
            }
            _ => {
                return Err(ParseSQLError::GenericError(format!(
                    "rule {:?} not valid",
                    pair.as_rule()
                )))
            }
        }

        match (&filter_left, &logical, &filter_right) {
            // if we have all three items, we must combine
            (Some(_fl), Some(_l), Some(_r)) => {
                if final_op.is_none() {
                    final_op = Some(combine(
                        filter_left.take().unwrap(),
                        logical.take().unwrap(),
                        filter_right.take().unwrap(),
                    )?);
                    // now that we combine the previous three elements
                    // the right side will be our new structure
                    filter_right = Some(final_op.take().unwrap());
                }
            }
            _ => continue,
        }
    }

    if let Some(f) = final_op {
        Ok(f)
    } else {
        Ok(filter_right.unwrap())
    }
}

fn combine(
    filter_left: LogicalOperator,
    op: BooleanOp,
    filter_right: LogicalOperator,
) -> Result<LogicalOperator, ParseSQLError> {
    match (filter_left, op, filter_right) {
        (left, BooleanOp::And, right) => Ok(LogicalOperator::And(Box::new(left), Box::new(right))),

        (left, BooleanOp::Or, right) => Ok(LogicalOperator::Or(Box::new(left), Box::new(right))),
    }
}

fn parse_logical_operator(pair: Pair<Rule>) -> Result<BooleanOp, ParseSQLError> {
    match pair.as_str().to_lowercase().as_str() {
        "and" => Ok(BooleanOp::And),
        "or" => Ok(BooleanOp::Or),
        _ => Err(ParseSQLError::GenericError(format!(
            "invalid operator {:?}",
            pair
        ))),
    }
}

fn parse_filter(mut pair: Pairs<Rule>) -> Result<Filter, ParseSQLError> {
    //id, condition, val
    let id = pair.next().unwrap().as_span().as_str().to_string();
    let condition = match pair.next().unwrap().as_str() {
        "=" => Operator::Eq,
        "!=" => Operator::NotEq,
        "<" => Operator::Less,
        "<=" => Operator::LessOrEq,
        ">=" => Operator::GreaterOrEq,
        ">" => Operator::Greater,
        _ => {
            return Err(ParseSQLError::InvalidOperator(format!(
                "operator {:?} is not valid",
                pair.as_str()
            )))
        }
    };
    let val = parse_val(pair.next().unwrap().into_inner())?;

    Ok(Filter {
        key: id,
        value: val,
        operator: condition,
    })
}

fn parse_values(pairs: Pairs<Rule>) -> Result<Vec<Value>, ParseSQLError> {
    let mut vec = Vec::new();
    for pair in pairs {
        vec.push(parse_val(pair.into_inner())?);
    }
    Ok(vec)
}

fn parse_val(mut pairs: Pairs<Rule>) -> Result<Value, ParseSQLError> {
    let pair = pairs.next().unwrap();
    let val_as_str = pair.as_span().as_str();
    let val = match pair.as_rule() {
        Rule::integer => Value::Int(val_as_str.parse::<i64>().map_err(|_e| {
            ParseSQLError::InvalidInteger(format!("{val_as_str} is not a valid int"))
        })?),
        Rule::float => Value::Float(val_as_str.parse::<f64>().map_err(|_| {
            ParseSQLError::InvalidFloat(format!("{val_as_str} is not a valid float"))
        })?),
        Rule::string => Value::String(pair.into_inner().next().unwrap().as_str().to_string()),
        _ => panic!("todo"),
    };

    Ok(val)
}

fn parse_columns(pairs: Pairs<Rule>) -> Result<Vec<String>, ParseSQLError> {
    Ok(pairs.map(|e| e.as_str().to_string()).collect())
}

fn parse_table_name(mut pair: Pairs<Rule>) -> Result<String, ParseSQLError> {
    let table = pair.next().unwrap().as_str().to_string();
    Ok(table)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse_create() {
        let command = "create table aa columns (abc int, abd String);";
        let sql = parse(command).unwrap();
        match sql {
            SqlCommand::Create {
                table,
                columns,
                column_types,
            } => {
                assert_eq!("aa", table.as_str());
                assert_eq!(vec!["abc".to_string(), "abd".to_string()], columns);
                assert_eq!(vec![ValueType::Int, ValueType::String], column_types);
            }
            _ => panic!("command should be insert"),
        }
    }

    #[test]
    fn test_parse_drop() {
        let command = "drop table aa;";
        let sql = parse(command).unwrap();
        match sql {
            SqlCommand::Drop { table } => {
                assert_eq!("aa", table.as_str());
            }
            _ => panic!("command should be drop"),
        }
    }

    // Test different combinations of update

    #[test]
    fn test_parse_update_without_where() {
        let command = "update a_table set col1='a', col2=1;";
        let sql = parse(command).unwrap();
        match sql {
            SqlCommand::Update {
                table,
                columns,
                values,
                filters,
            } => {
                assert_eq!("a_table", table.as_str());
                assert_eq!(vec!["col1".to_string(), "col2".to_string()], columns);
                assert_eq!(vec![Value::String("a".into()), Value::Int(1)], values);
                assert_eq!(None, filters);
            }
            _ => panic!("command should be update"),
        }
    }

    #[test]
    fn test_parse_update_with_where() {
        let command = "update a_table set col1='a', col2=1 where a=1;";
        let sql = parse(command).unwrap();
        let expected_filter = LogicalOperator::Filter(Filter {
            key: "a".into(),
            value: Value::Int(1),
            operator: Operator::Eq,
        });
        match sql {
            SqlCommand::Update {
                table,
                columns,
                values,
                filters,
            } => {
                assert_eq!("a_table", table.as_str());
                assert_eq!(vec!["col1".to_string(), "col2".to_string()], columns);
                assert_eq!(vec![Value::String("a".into()), Value::Int(1)], values);
                assert_eq!(Some(expected_filter), filters);
            }
            _ => panic!("command should be update"),
        }
    }

    // Test different combinations of the insert
    #[test]
    fn test_parse_insert() {
        let command = "insert into a_table (col1, col2) values ('a', 1);";
        let sql = parse(command).unwrap();
        match sql {
            SqlCommand::Insert {
                table,
                columns,
                values,
            } => {
                assert_eq!("a_table", table.as_str());
                assert_eq!(vec!["col1".to_string(), "col2".to_string()], columns);
                assert_eq!(vec![Value::String("a".into()), Value::Int(1)], values);
            }
            _ => panic!("command should be insert"),
        }
    }

    // Test different combinations of the delete stmt

    #[test]
    fn test_parse_delete_without_where() {
        let command = "delete from a_table;";
        let sql = parse(command).unwrap();
        match sql {
            SqlCommand::Delete { table, filters } => {
                assert_eq!("a_table", table.as_str());
                assert_eq!(None, filters);
            }
            _ => panic!("command should be delete"),
        }
    }

    #[test]
    fn test_parse_delete_with_where() {
        let command = "delete from a_table where a=1;";
        let sql = parse(command).unwrap();

        let expected_filters = LogicalOperator::Filter(Filter {
            key: "a".into(),
            value: Value::Int(1),
            operator: Operator::Eq,
        });

        match sql {
            SqlCommand::Delete { table, filters } => {
                assert_eq!("a_table", table.as_str());
                assert_eq!(Some(expected_filters), filters);
            }
            _ => panic!("command should be delete"),
        }
    }

    #[test]
    fn test_parse_delete_with_where_float() {
        let command = "delete from a_table where a=1.3;";
        let sql = parse(command).unwrap();

        let expected_filters = LogicalOperator::Filter(Filter {
            key: "a".into(),
            value: Value::Float(1.3),
            operator: Operator::Eq,
        });

        match sql {
            SqlCommand::Delete { table, filters } => {
                assert_eq!("a_table", table.as_str());
                assert_eq!(Some(expected_filters), filters);
            }
            _ => panic!("command should be delete"),
        }
    }

    #[test]
    fn test_parse_delete_with_where_string() {
        let command = "delete from a_table where a='abacaxi';";
        let sql = parse(command).unwrap();

        let expected_filters = LogicalOperator::Filter(Filter {
            key: "a".into(),
            value: Value::String("abacaxi".into()),
            operator: Operator::Eq,
        });

        match sql {
            SqlCommand::Delete { table, filters } => {
                assert_eq!("a_table", table.as_str());
                assert_eq!(Some(expected_filters), filters);
            }
            _ => panic!("command should be delete"),
        }
    }

    // Test different combinations of the select stmt

    #[test]
    fn test_parse_select_single_column_command() {
        let command = "select column from my_table;";
        let sql = parse(command).unwrap();
        match sql {
            SqlCommand::Select {
                columns,
                filters,
                table,
                group_by: _,
            } => {
                assert_eq!(vec!["column".to_string()], columns);
                assert_eq!(None, filters);
                assert_eq!("my_table", table.as_str());
            }
            _ => panic!("command should be a select"),
        };
    }

    #[test]
    fn test_parse_select_multiple_columns_command() {
        let command = "Select column, column2, column3 from my_table;";
        let sql = parse(command).unwrap();
        match sql {
            SqlCommand::Select {
                columns,
                filters,
                table,
                group_by: _,
            } => {
                assert_eq!(
                    vec![
                        "column".to_string(),
                        "column2".to_string(),
                        "column3".to_string()
                    ],
                    columns
                );
                assert_eq!(None, filters);
                assert_eq!("my_table", table.as_str());
            }
            _ => panic!("command should be a select"),
        };
    }

    #[test]
    fn test_parse_select_with_group_by() {
        let command = "select column from my_table group by column;";
        let sql = parse(command).unwrap();
        match sql {
            SqlCommand::Select {
                columns,
                filters,
                table,
                group_by,
            } => {
                assert_eq!(vec!["column".to_string()], columns);
                assert_eq!(None, filters);
                assert_eq!("my_table", table.as_str());
                assert_eq!(Some(vec!["column".to_string()]), group_by);
            }
            _ => panic!("command should be a select"),
        };
    }

    #[test]
    fn test_parse_select_with_where() {
        let command = "Select column, column2, column3 from my_table where a=1;";
        let sql = parse(command).unwrap();
        match sql {
            SqlCommand::Select {
                columns,
                filters,
                table,
                group_by: _,
            } => {
                assert_eq!(
                    vec![
                        "column".to_string(),
                        "column2".to_string(),
                        "column3".to_string()
                    ],
                    columns
                );
                assert_eq!(
                    Some(LogicalOperator::Filter(Filter {
                        key: "a".into(),
                        value: Value::Int(1),
                        operator: Operator::Eq,
                    })),
                    filters
                );
                assert_eq!("my_table", table.as_str());
            }
            _ => panic!("command should be a select"),
        };
    }

    #[test]
    fn test_parse_select_where_with_multiple_filters() {
        let command = "Select column from my_table where a=1 and b=2 or c=3;";
        let sql = parse(command).unwrap();

        let a = LogicalOperator::Filter(Filter {
            key: "a".into(),
            value: Value::Int(1),
            operator: Operator::Eq,
        });

        let b = LogicalOperator::Filter(Filter {
            key: "b".into(),
            value: Value::Int(2),
            operator: Operator::Eq,
        });

        let c = LogicalOperator::Filter(Filter {
            key: "c".into(),
            value: Value::Int(3),
            operator: Operator::Eq,
        });

        let expected_filters = LogicalOperator::And(
            Box::new(a),
            Box::new(LogicalOperator::Or(Box::new(b), Box::new(c))),
        );

        match sql {
            SqlCommand::Select {
                columns,
                filters,
                table,
                group_by: _,
            } => {
                assert_eq!(vec!["column".to_string(),], columns);
                assert_eq!(Some(expected_filters), filters);
                assert_eq!("my_table", table.as_str());
            }
            _ => panic!("command should be a select"),
        };
    }

    #[test]
    fn test_parse_select_where_with_grouped_condition() {
        let command = "Select column from my_table where a=1 and (b=2 or c=3);";
        let sql = parse(command).unwrap();

        let a = LogicalOperator::Filter(Filter {
            key: "a".into(),
            value: Value::Int(1),
            operator: Operator::Eq,
        });

        let b = LogicalOperator::Filter(Filter {
            key: "b".into(),
            value: Value::Int(2),
            operator: Operator::Eq,
        });

        let c = LogicalOperator::Filter(Filter {
            key: "c".into(),
            value: Value::Int(3),
            operator: Operator::Eq,
        });

        let grouped = LogicalOperator::GroupedCondition(Box::new(LogicalOperator::Or(
            Box::new(b),
            Box::new(c),
        )));

        let expected_filters = LogicalOperator::And(Box::new(a), Box::new(grouped));

        match sql {
            SqlCommand::Select {
                columns,
                filters,
                table,
                group_by: _,
            } => {
                assert_eq!(vec!["column".to_string(),], columns);
                assert_eq!(Some(expected_filters), filters);
                assert_eq!("my_table", table.as_str());
            }
            _ => panic!("command should be a select"),
        };
    }
}
