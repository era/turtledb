use crate::sql::types::SqlCommand;
use crate::sql::types::{Column, Table, ValueType};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::sync::{Mutex, RwLock};

mod storage;

#[derive(Debug)]
struct WAL {
    pub op: Vec<String>,
    file: File,
}

#[derive(Debug)]
pub struct Database {
    wal: Mutex<WAL>,
    schema: RwLock<HashMap<String, RwLock<Table>>>,
}

impl WAL {
    pub fn new(path: &Path) -> Result<Self, String> {
        let file = std::fs::OpenOptions::new()
            .write(true)
            .append(true)
            .create(true)
            .open(path)
            .expect("could not create or open the wal log file");

        Ok(Self { op: vec![], file })
    }

    pub fn write(&mut self, command: &str) -> Result<(), String> {
        self.file
            .write(command.as_bytes())
            .map_err(|e| e.to_string())?;
        self.file.flush()..map_err(|e| e.to_string())?;
        self.op.push(command.to_string());
        Ok(())
    }
}

impl Database {
    fn new(wal: WAL) -> Self {
        Self {
            wal: Mutex::new(wal),
            schema: RwLock::new(HashMap::new()),
        }
    }

    /// Important to note that executing a DDL will block all other operations on the database.
    pub fn execute_ddl(self, sql: SqlCommand, raw_command: &str) -> Result<(), String> {
        match sql {
            SqlCommand::Create {
                table,
                column_types,
                columns,
            } => self.create_table(table, column_types, columns, raw_command)?,
            SqlCommand::Drop { table } => self.drop_table(table, raw_command)?,
            //TODO create a way to update tables
            _ => return Err("SqlCommand is not a valid DDL".into()),
        };
        Ok(())
    }

    fn create_table(
        &self,
        name: String,
        types: Vec<ValueType>,
        columns: Vec<String>,
        raw_command: &str,
    ) -> Result<(), String> {
        let mut schema = self.schema.write().unwrap();

        if schema.contains_key(&name) {
            return Err("table already exists".into());
        }

        let table_columns = Vec::with_capacity(columns.len());
        let indexes = Vec::new();

        for i in 0..columns.len() {
            let column = Column {
                name: columns.get(i).unwrap().into(),
                col_type: types.get(i).unwrap().clone(),
                //todo get indexes
                is_index: false,
            };
            //todo for now indexes will be empty
        }

        let table = Table {
            name: name.clone(),
            columns: table_columns,
            indexes,
        };

        schema.insert(name, RwLock::new(table));
        //TODO setup storage (table file)
        self.wal.lock().unwrap().write(raw_command)?;
        Ok(())
    }

    fn drop_table(&self, name: String, raw_command: &str) -> Result<(), String> {
        let mut schema = self.schema.write().unwrap();

        if !schema.contains_key(&name) {
            return Err("table does not exist".into());
        }

        schema.remove(&name);
        self.wal.lock().unwrap().write(raw_command)?;
        //TODO drop storage (table file)
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::fs;
    use tempdir::TempDir;

    fn setup_db(log: &Path) -> Database {
        let wal = WAL::new(log).unwrap();
        Database::new(wal)
    }

    #[test]
    fn test_create_table() {
        let root = TempDir::new("db").unwrap();
        let log = root.path().join("wal.log");
        let db = setup_db(log.as_path());

        let table = "my_table";
        let column_types = vec![ValueType::Float, ValueType::Int];
        let column_names = vec!["something".to_string(), "else".to_string()];
        let raw_sql = "create table my_table column (something float, else int);";

        db.create_table(table.into(), column_types, column_names, raw_sql)
            .unwrap();
        let wal_content = fs::read_to_string(log).expect("Should have been able to read the file");

        assert_eq!(raw_sql, wal_content);
    }

    #[test]
    fn test_delete_table() {
        let root = TempDir::new("db").unwrap();
        let log = root.path().join("wal.log");
        let db = setup_db(log.as_path());

        db.schema.write().unwrap().insert(
            "my_table".into(),
            RwLock::new(Table {
                name: "my_table".into(),
                columns: vec![],
                indexes: vec![],
            }),
        );

        let raw_sql = "drop table my_table;";

        db.drop_table("my_table".into(), raw_sql).unwrap();
        let wal_content = fs::read_to_string(log).expect("Should have been able to read the file");

        assert_eq!(raw_sql, wal_content);
    }
}
