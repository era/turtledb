// Turtle uses Apache Arrow (https://arrow.apache.org/docs/format/Columnar.html)
// to store data locally.
// Apache Arrow is column-based and not row based, which allow us to quickly fetch a whole
// column (select my_col from table), but it's slower to fetch entire rows (select * from table).
