use crate::config::{Limits, ThrottlingConfig};
use chrono::prelude::*;
use std::collections::HashMap;
use std::sync::Mutex;

#[derive(Debug, PartialEq, Clone, Hash)]
pub enum ThrottledReason {
    /// the max_total_requests limit has been reached
    TotalRequests,
    /// the key used all their bucket tokens
    PerKey,
    /// the key used all their tokens for the minute
    PerMinute,
}

#[derive(Debug)]
pub struct ThrottlingManager {
    limits: ThrottlingConfig,
    current_requests: Mutex<CurrentRequests>,
}
#[derive(Default, PartialEq, Debug)]
struct CurrentRequests {
    //TODO implement ability to override limits for certain accounts
    current_total_requests: i32,
    current_requests_per_key: HashMap<String, i32>,
    current_requests_per_minute_per_key: HashMap<String, Vec<i64>>,
}

impl CurrentRequests {
    /// clean any timestamp for the key that are older than one minute
    fn clean_old_timestamps(&mut self, key: &str, time: i64) {
        let tokens = self.current_requests_per_minute_per_key.get(key);
        if let Some(vec) = tokens {
            let vec = vec.iter().filter(|t| **t >= time).copied().collect();
            self.current_requests_per_minute_per_key
                .insert(key.to_string(), vec);
        } else {
            self.current_requests_per_minute_per_key
                .insert(key.to_string(), Vec::new());
        }
    }

    /// gets a token from the per minute bucket for key
    /// if no token available returns an error
    fn get_token_from_timestamp_bucket(
        &mut self,
        key: &str,
        limits: &Limits,
    ) -> Result<(), ThrottledReason> {
        let now = Utc::now();
        let one_minute_ago = now - chrono::Duration::minutes(1);
        self.clean_old_timestamps(key, one_minute_ago.timestamp());
        // Thanks to the previous method we know we have something there so unwrap is safe
        let bucket = self
            .current_requests_per_minute_per_key
            .get_mut(key)
            .unwrap();
        if bucket.len() >= limits.max_total_requests_per_minute_per_key as usize {
            Err(ThrottledReason::PerMinute)
        } else {
            bucket.push(now.timestamp());

            Ok(())
        }
    }

    /// put token back from the total requests and total requests per key
    fn put_token_back(&mut self, key: &str) {
        self.current_total_requests -= 1;
        let requests_per_key = self.current_requests_per_key.get_mut(key).unwrap();
        *requests_per_key -= 1;
    }

    /// checks the limits and if they are not ok, return an error
    /// if everything is fine, it takes a token from the bucket
    fn get_token_from_bucket(
        &mut self,
        key: String,
        limits: &Limits,
    ) -> Result<(), ThrottledReason> {
        // even if we are going to refuse this request, we should remove a token
        // from the minutes request from this user
        self.get_token_from_timestamp_bucket(&key, limits)?;

        if self.current_total_requests >= limits.max_total_requests {
            return Err(ThrottledReason::TotalRequests);
        }

        if let Some(total_requests) = self.current_requests_per_key.get_mut(&key) {
            if *total_requests >= limits.max_total_requests_per_key {
                return Err(ThrottledReason::PerKey);
            }
            *total_requests += 1;
        } else {
            self.current_requests_per_key.insert(key, 1);
        }
        self.current_total_requests += 1;

        Ok(())
    }
}

impl ThrottlingManager {
    pub fn new(
        max_total_requests: i32,
        max_total_requests_per_key: i32,
        max_total_requests_per_minute_per_key: i32,
    ) -> Self {
        let limits = Limits {
            max_total_requests,
            max_total_requests_per_key,
            max_total_requests_per_minute_per_key,
            override_max_total_requests_per_key: HashMap::default(),
            override_max_total_requests_per_minute_per_key: HashMap::default(),
        };

        Self {
            limits: ThrottlingConfig::Limit(limits),
            current_requests: Mutex::new(CurrentRequests::default()),
        }
    }
    /// tries to execute f, if the number of requests for key or the total amount
    /// are less than the configured limits
    pub fn execute<F>(&self, key: String, f: Box<dyn FnOnce() -> F>) -> Result<F, ThrottledReason> {
        self.verify(key.clone())?;

        let result = Ok(f());

        self.release_tokens(&key);
        result
    }
    /// release any relevant token (e.g. total current requests)
    /// tokens based on time won't be released with this method call
    fn release_tokens(&self, key: &str) {
        let mut curr = self.current_requests.lock().unwrap();
        curr.put_token_back(key);
    }

    /// checks the limits and if they are not ok, return an error
    /// if everything is fine, it takes a token from the bucket
    fn verify(&self, key: String) -> Result<(), ThrottledReason> {
        match &self.limits {
            ThrottlingConfig::NoLimits => Ok(()),
            ThrottlingConfig::Limit(limits) => {
                let mut curr = self.current_requests.lock().unwrap();
                curr.get_token_from_bucket(key, limits)
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    //TODO tests for key override on all the different limits

    #[test]
    fn test_total_requests_success_per_minute() {
        let five_minutes_ago = chrono::Utc::now() - chrono::Duration::minutes(5);
        let current_requests = CurrentRequests {
            current_total_requests: 4,
            current_requests_per_key: HashMap::new(),
            current_requests_per_minute_per_key: HashMap::from([(
                "fake_key".into(),
                vec![
                    five_minutes_ago.timestamp(),
                    five_minutes_ago.timestamp(),
                    five_minutes_ago.timestamp(),
                ],
            )]),
        };
        let limits = Limits {
            max_total_requests: 5,
            max_total_requests_per_key: 1,
            max_total_requests_per_minute_per_key: 1,
            override_max_total_requests_per_key: HashMap::default(),
            override_max_total_requests_per_minute_per_key: HashMap::default(),
        };
        let manager = ThrottlingManager {
            limits: ThrottlingConfig::Limit(limits),
            current_requests: Mutex::new(current_requests),
        };

        let res = manager.execute("fake_key".into(), Box::new(|| {}));
        assert_eq!(Ok(()), res);
        assert_eq!(
            1_usize,
            manager
                .current_requests
                .lock()
                .unwrap()
                .current_requests_per_minute_per_key
                .get("fake_key")
                .unwrap()
                .len()
        );
    }

    #[test]
    fn test_total_requests_failed_per_minute() {
        let now = chrono::Utc::now();
        let current_requests = CurrentRequests {
            current_total_requests: 4,
            current_requests_per_key: HashMap::new(),
            current_requests_per_minute_per_key: HashMap::from([(
                "fake_key".into(),
                vec![now.timestamp(), now.timestamp(), now.timestamp()],
            )]),
        };
        let limits = Limits {
            max_total_requests: 5,
            max_total_requests_per_key: 1,
            max_total_requests_per_minute_per_key: 3,
            override_max_total_requests_per_key: HashMap::default(),
            override_max_total_requests_per_minute_per_key: HashMap::default(),
        };
        let manager = ThrottlingManager {
            limits: ThrottlingConfig::Limit(limits),
            current_requests: Mutex::new(current_requests),
        };

        let res = manager.execute("fake_key".into(), Box::new(|| {}));
        assert_eq!(Err(ThrottledReason::PerMinute), res);
        assert_eq!(
            3_usize,
            manager
                .current_requests
                .lock()
                .unwrap()
                .current_requests_per_minute_per_key
                .get("fake_key")
                .unwrap()
                .len()
        );
    }

    #[test]
    fn test_total_requests_limit() {
        let current_requests = CurrentRequests {
            current_total_requests: 5,
            current_requests_per_key: HashMap::new(),
            current_requests_per_minute_per_key: HashMap::new(),
        };
        let limits = Limits {
            max_total_requests: 5,
            max_total_requests_per_key: 1,
            max_total_requests_per_minute_per_key: 10,
            override_max_total_requests_per_key: HashMap::default(),
            override_max_total_requests_per_minute_per_key: HashMap::default(),
        };
        let manager = ThrottlingManager {
            limits: ThrottlingConfig::Limit(limits),
            current_requests: Mutex::new(current_requests),
        };

        let res = manager.execute("fake_key".into(), Box::new(|| {}));
        assert_eq!(Err(ThrottledReason::TotalRequests), res);
        assert_eq!(
            5,
            manager
                .current_requests
                .lock()
                .unwrap()
                .current_total_requests
        );
    }

    #[test]
    fn test_total_requests_limit_success() {
        let current_requests = CurrentRequests {
            current_total_requests: 4,
            current_requests_per_key: HashMap::new(),
            current_requests_per_minute_per_key: HashMap::new(),
        };
        let limits = Limits {
            max_total_requests: 5,
            max_total_requests_per_key: 1,
            max_total_requests_per_minute_per_key: 10,
            override_max_total_requests_per_key: HashMap::default(),
            override_max_total_requests_per_minute_per_key: HashMap::default(),
        };
        let manager = ThrottlingManager {
            limits: ThrottlingConfig::Limit(limits),
            current_requests: Mutex::new(current_requests),
        };

        let res = manager.execute("fake_key".into(), Box::new(|| 1));
        assert_eq!(Ok(1), res);
        assert_eq!(
            4,
            manager
                .current_requests
                .lock()
                .unwrap()
                .current_total_requests
        );
    }

    #[test]
    fn test_total_requests_per_key_limit() {
        let current_requests = CurrentRequests {
            current_total_requests: 4,
            current_requests_per_key: HashMap::from([("fake_key".into(), 1)]),
            current_requests_per_minute_per_key: HashMap::new(),
        };
        let limits = Limits {
            max_total_requests: 5,
            max_total_requests_per_key: 1,
            max_total_requests_per_minute_per_key: 10,
            override_max_total_requests_per_key: HashMap::default(),
            override_max_total_requests_per_minute_per_key: HashMap::default(),
        };
        let manager = ThrottlingManager {
            limits: ThrottlingConfig::Limit(limits),
            current_requests: Mutex::new(current_requests),
        };

        let res = manager.execute("fake_key".into(), Box::new(|| {}));
        assert_eq!(Err(ThrottledReason::PerKey), res);
        assert_eq!(
            4,
            manager
                .current_requests
                .lock()
                .unwrap()
                .current_total_requests
        );
        assert_eq!(
            1,
            *manager
                .current_requests
                .lock()
                .unwrap()
                .current_requests_per_key
                .get("fake_key")
                .unwrap()
        );
    }

    #[test]
    fn test_total_requests_per_key_limit_success() {
        let current_requests = CurrentRequests {
            current_total_requests: 4,
            current_requests_per_key: HashMap::from([("fake_key".into(), 1)]),
            current_requests_per_minute_per_key: HashMap::new(),
        };
        let limits = Limits {
            max_total_requests: 5,
            max_total_requests_per_key: 2,
            max_total_requests_per_minute_per_key: 10,
            override_max_total_requests_per_key: HashMap::default(),
            override_max_total_requests_per_minute_per_key: HashMap::default(),
        };
        let manager = ThrottlingManager {
            limits: ThrottlingConfig::Limit(limits),
            current_requests: Mutex::new(current_requests),
        };

        let res = manager.execute("fake_key".into(), Box::new(|| 1));
        assert_eq!(Ok(1), res);
        assert_eq!(
            4,
            manager
                .current_requests
                .lock()
                .unwrap()
                .current_total_requests
        );
        assert_eq!(
            1,
            *manager
                .current_requests
                .lock()
                .unwrap()
                .current_requests_per_key
                .get("fake_key")
                .unwrap()
        );
    }
}
