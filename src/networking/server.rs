// for now the service will use http connections
// we can explore QUIC as well
// the authentication will be "Basic" HTTP authentication
// we will throttled based on user_id and max number of connections

use once_cell::sync::OnceCell;
use std::convert::Infallible;

use crate::auth;
use hyper::header::HeaderValue;

use hyper::{Body, Request, Response};

use crate::error::NetworkError;

use crate::networking::throttling::ThrottlingManager;

pub static EXECUTOR: OnceCell<Executor> = OnceCell::new();

fn not_authorized() -> Response<Body> {
    Response::builder()
        .status(401)
        .body("Not authorized".into())
        .unwrap()
}

fn too_many_requests() -> Response<Body> {
    Response::builder()
        .status(429)
        .body("Too many requests".into())
        .unwrap()
}

pub trait Server {
    fn serve(self) -> Result<(), NetworkError>;
}

#[derive(Debug)]
pub struct Executor {
    throttling_manager: ThrottlingManager,
    authenticator: auth::Authenticator,
}

impl Executor {
    pub fn new() -> Self {
        Self {
            throttling_manager: ThrottlingManager::new(0, 0, 0),
            //TODO
            authenticator: auth::Authenticator {},
        }
    }
    /// execute the http request checking if we should first throttle the request
    pub async fn execute(&self, req: Request<Body>) -> Result<Response<Body>, Infallible> {
        let credentials =
            match extract_http_authorization_header(req.headers().get("Authorization")) {
                Ok(c) => c,
                _ => {
                    log::debug!("no authorization header for request");
                    return Ok(not_authorized());
                }
            };

        let user = match self
            .authenticator
            .authenticate(auth::Credentials::HTTP(credentials))
        {
            Ok(u) => u,
            _ => {
                log::debug!("wrong user/password");
                return Ok(not_authorized());
            }
        };

        match self
            .throttling_manager
            .execute(user.name, Box::new(|| execute(req)))
        {
            Ok(r) => r,
            Err(_e) => Ok(too_many_requests()),
        }
    }
}

fn extract_http_authorization_header(_header: Option<&HeaderValue>) -> Result<String, ()> {
    todo!()
}

/// execute handles all requests to our server
/// and calls the correct handler.
fn execute(_req: Request<Body>) -> Result<Response<Body>, Infallible> {
    Ok(Response::new("Hello, World".into()))
}
