use crate::error::NetworkError;
use log;
use std::convert::Infallible;
use std::net::SocketAddr;

use crate::networking::server;
use hyper::service::{make_service_fn, service_fn};
use hyper::Server;

use tokio::runtime::Runtime;

#[derive(Debug, PartialEq, Clone, Hash)]
pub struct TcpServer {
    pub address: SocketAddr,
}

impl TcpServer {
    fn new(address: SocketAddr) -> Result<Self, NetworkError> {
        Ok(Self { address })
    }
}

impl server::Server for TcpServer {
    fn serve(self) -> Result<(), NetworkError> {
        let rt = Runtime::new().map_err(|e| NetworkError::GenericError(e.to_string()))?;
        let addr = &self.address;
        // if already set, it will return the value in it
        // so we don't really care about this Result
        server::EXECUTOR.set(server::Executor::new());

        rt.block_on(async {
            // For every connection, we must make a `Service` to handle all
            // incoming HTTP requests on said connection.
            let make_svc = make_service_fn(|_conn| {
                // This is the `Service` that will handle the connection.
                // `service_fn` is a helper to convert a function that
                // returns a Response into a `Service`.
                async {
                    Ok::<_, Infallible>(service_fn(|req| {
                        server::EXECUTOR.get().unwrap().execute(req)
                    }))
                }
            });
            log::info!("serving requests on the address {:?}", addr);
            let server = Server::bind(addr).serve(make_svc);

            match server.await {
                Ok(_) => Ok(()),
                Err(e) => {
                    log::error!("{:?}", e);
                    Err(e)
                }
            }
        })
        .map_err(|e| NetworkError::GenericError(e.to_string()))
    }
}
