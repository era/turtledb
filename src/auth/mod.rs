use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, PartialEq, Clone, Hash, Debug)]
pub enum Credentials {
    HTTP(String),
}

#[derive(Serialize, Deserialize, PartialEq, Clone, Hash, Debug)]
pub enum AuthenticationError {
    USER_DOES_NOT_EXIST,
    WRONG_PASSWORD,
    INVALID_AUTHENTICATION_METHOD,
}

#[derive(Serialize, Deserialize, PartialEq, Clone, Hash, Debug)]
pub struct Authenticator;

#[derive(Serialize, Deserialize, PartialEq, Clone, Hash, Debug)]
pub struct User {
    pub name: String,
}

impl Authenticator {
    pub fn authenticate(&self, _authentication: Credentials) -> Result<User, AuthenticationError> {
        todo!()
    }
}
