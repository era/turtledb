// all configuration for now will be in a config file
// later we will use our own database to store this information

use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Configuration {
    throttling: Option<Limits>,
}

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub struct Limits {
    pub max_total_requests: i32,
    pub max_total_requests_per_key: i32,
    pub max_total_requests_per_minute_per_key: i32,
    pub override_max_total_requests_per_key: HashMap<String, i32>,
    pub override_max_total_requests_per_minute_per_key: HashMap<String, i32>,
}

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub enum ThrottlingConfig {
    NoLimits,
    Limit(Limits),
}

impl Configuration {
    pub fn get_throttling_config(&self) -> ThrottlingConfig {
        match &self.throttling {
            None => ThrottlingConfig::NoLimits,
            Some(limits) => ThrottlingConfig::Limit(limits.clone()),
        }
    }
}
