use std::error::Error;
use std::fmt;
use std::fmt::Display;

/// syntax and grammar errors while parsing
/// a string to a sql command
#[derive(Debug)]
pub enum ParseSQLError {
    GenericError(String),
    InvalidOperator(String),
    InvalidInteger(String),
    InvalidFloat(String),
}

#[derive(Debug)]
pub enum ThreadPoolError {
    GenericError(String),

    //ThreadPool
    CouldNotScheduleJob(String),
}

#[derive(Debug)]
pub enum NetworkError {
    GenericError(String),
}

impl Display for ThreadPoolError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "error {}", self)
    }
}

impl Display for ParseSQLError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "error while parsing sql {}", self)
    }
}

impl Display for NetworkError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "error while parsing sql {}", self)
    }
}

impl Error for ParseSQLError {}
impl Error for ThreadPoolError {}
impl Error for NetworkError {}
