// this is not used in the project
// it's here only because I wanted to play a bit with the concept of thread pool
// manually. It's a reference for channels and other things for me.
// You can safely ignore this
use crate::error::ThreadPoolError;
use crossbeam::channel::{self, Receiver, Sender};

pub type Task = Box<dyn FnOnce() + Send + 'static>;

pub trait ThreadPool {
    fn spawn<T>(&self, task: T) -> Result<(), ThreadPoolError>
    where
        T: FnOnce() + Send + 'static;
}

enum WorkerMessage {
    Job(Task),
    Shutdown,
}

pub struct NaiveThreadPool {
    pool: Vec<Worker>,
    tx: Sender<WorkerMessage>,
    rx: Receiver<WorkerMessage>,
}

struct Worker {
    thread: Option<std::thread::JoinHandle<()>>,
}

impl NaiveThreadPool {
    pub fn new(size: usize) -> Result<Self, ThreadPoolError> {
        let mut pool = Vec::with_capacity(size);

        let (tx, rx): (Sender<WorkerMessage>, Receiver<WorkerMessage>) = channel::unbounded();
        for _ in 0..size {
            pool.push(Worker::new(rx.clone()));
        }

        Ok(Self { pool, tx, rx })
    }
}

impl ThreadPool for NaiveThreadPool {
    fn spawn<T>(&self, task: T) -> Result<(), ThreadPoolError>
    where
        T: FnOnce() + Send + 'static,
    {
        self.tx
            .send(WorkerMessage::Job(Box::new(task)))
            .map_err(|e| ThreadPoolError::CouldNotScheduleJob(e.to_string()))?;

        Ok(())
    }
}

impl Drop for NaiveThreadPool {
    fn drop(&mut self) {
        for _ in &self.pool {
            //TODO handle this better
            //TODO handle error
            self.tx.send(WorkerMessage::Shutdown);
        }
        for worker in &mut self.pool {
            if let Some(thread) = worker.thread.take() {
                // handle error
                thread.join();
            }
        }
    }
}

impl Worker {
    fn new(rx: Receiver<WorkerMessage>) -> Worker {
        let thread = std::thread::spawn(move || loop {
            let message = rx.recv().unwrap();
            match message {
                //TODO handle panic
                WorkerMessage::Job(task) => task(),
                WorkerMessage::Shutdown => break,
            }
        });

        Worker {
            thread: Some(thread),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn create_thread_pool() {
        let thread_pool = NaiveThreadPool::new(5).unwrap();
        assert_eq!(5, thread_pool.pool.len());
    }

    #[test]
    fn send_tasks() {
        let thread_pool = NaiveThreadPool::new(2).unwrap();

        let (tx, rx): (Sender<i32>, Receiver<i32>) = channel::unbounded();
        thread_pool
            .spawn(move || {
                tx.send(1).unwrap();
            })
            .unwrap();

        let result = rx.recv().unwrap();
        assert_eq!(1, result);
    }
}
