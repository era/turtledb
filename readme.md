# TurtleDB

TurtleDB is a toy database made to understand better how databases and Rust work. Still work in progress.

This is a big excuse to try out things or apply things I have read on papers. So you should see a bit of verything here.

If something comes from a paper I will note on the code itself and in this readme.

The Database itself is a HTTP service that responds to a sub-set of SQL standard.
Build into the system there is:
- Throttling mechanism: per user, per total requests and a quota per minute per user (https://en.wikipedia.org/wiki/Token_bucket)
- Authentication (Using Basic Http Authentication)
- ...

## Database

### SQL and parser

The sub-set support by this implementation can be seen either in the tests at
`src/sql/mod.rs` or the grammar `src/sql/grammar.pest`.  For now we the database has only three type of values (string, int and float). The db does not support joins as it's.

We use a PEG library (`pest`) to define the grammar and create the AST.

### Replication

### Sharding

### Storage

## Server

### P2P